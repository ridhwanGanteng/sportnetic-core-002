package com.sportnetic.master.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sportnetic.feign.dto.master.UserDto;
import com.sportnetic.master.service.UserImplService;
import com.sportnetic.repositories.master.entity.UserEntity;


@RestController
@RequestMapping(path = "/api/user", produces = MediaType.APPLICATION_JSON_VALUE)

public class UserCtrl {

	@Autowired
	private UserImplService userService;

	@RequestMapping(value = "/view/get-list-user/v.1", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<UserDto>> getListUser() throws Exception {
		return new ResponseEntity<List<UserDto>>(userService.getAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/view/get-list-user/v.1", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<UserDto>> getListUserGET() throws Exception {
		return new ResponseEntity<List<UserDto>>(userService.getAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/transaction/insert/v.1", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<UserDto>> saveOrUpdate(
			@RequestHeader(name = "Accept-Language", required = false) String locale,
			@RequestBody UserDto user) throws Exception {
		userService.saveOrUpdate(user);
		return new ResponseEntity<List<UserDto>>(userService.getAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/transaction/delete/v.1", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<UserDto>> delete(
			@RequestHeader(name = "Accept-Language", required = false) String locale,
			@RequestBody UserDto...user) throws Exception {
		userService.delete(user);
		return new ResponseEntity<List<UserDto>>(userService.getAll(), HttpStatus.OK);
	}

}
