package com.sportnetic.master.service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sportnetic.feign.dto.master.UserDto;
import com.sportnetic.repositories.master.dao.UserRepo;
import com.sportnetic.repositories.master.entity.UserEntity;

@Service("userImplService")
public class UserImplService {

	@Autowired
	private UserRepo userRepo;

	private UserEntity userEntity;
	private UserDto userDto;

	public List<UserDto> getAll() {
		List<UserDto> listUser = new ArrayList<UserDto>();
		for(UserEntity _userEntity: userRepo.findAll()) {
			listUser.add(_userEntity.parseDto());
		}
		return listUser;
	}

	public void validateSaveOrUpdate() throws Exception {
		if (this.userDto == null || this.userDto.getBirthDate() == null || this.userDto.getEmail() == null
				|| this.userDto.getEmail().length() <= 0 || this.userDto.getNama() == null
				|| this.userDto.getNama().length() <= 0 || this.userDto.getPassword() == null
				|| this.userDto.getPassword().length() <= 0) {
			throw new Exception();

		}
	}

	public void saveOrUpdate(UserDto user) throws Exception {
		this.userDto = user;
		this.validateSaveOrUpdate();
		if (user.getUserId() != null && user.getUserId().length() > 0) {
			update();
		} else if (user.getUserId() == null) {
			insert();
		}
	}

	@Transactional
	private void insert() {
		userEntity = new UserEntity(userDto);
		userRepo.save(userEntity);

	}

	@Transactional
	private void update() throws Exception {
		UUID uuid = UUID.fromString(userDto.getUserId());
		userEntity = userRepo.findByUserId(uuid);
		if (userEntity != null) {
			userEntity.setNama(userDto.getNama());
			userEntity.setBirthDate(userDto.getBirthDate());
			userEntity.setPassword(userDto.getPassword());
			userEntity.setGender(userDto.getGender());
			userRepo.save(userEntity);
			return;
		}
		throw new Exception();
	}

	private void validateDelete(UserDto... user) throws Exception {
		UUID uuid = null;
		for (UserDto _user : user) {
			uuid = UUID.fromString(_user.getUserId());
			UserEntity _userEntity = userRepo.findByUserId(uuid);
			if (_userEntity == null || _userEntity.getIsActive() || _userEntity.getUserId() == null) {
				throw new Exception();
			}
		}
	}

	@Transactional
	public void delete(UserDto... user) throws Exception {
		validateDelete(user);
		UUID uuid = null;
		for (UserDto _userDto : user) {
			uuid = UUID.fromString(_userDto.getUserId());
			UserEntity _userEntity = userRepo.findByUserId(uuid);
			_userEntity.setIsActive(false);

		}

	}
}
